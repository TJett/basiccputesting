# README #

* Tests are run with command 
* * `jlsCPUTester circuit.jls testname.a`


### How do I get set up? ###
* Create an account on Bitbucket
* git clone into repository
* * `git clone https://bitbucket.org/TJett/basiccputesting.git`
* Enter your username and password for Bitbucket at the prompt
* Make sure you add kurmasz' path for the test suite
* * `/home/kurmasz/public/CS451/bin/`
* You can append your path for this session by using the command
* * `PATH=$PATH:/home/kurmasz/public/CS451/bin/`

* If you wanted to keep that path permanent, add it to your `~/.bash_profile` and source
* * `PATH=$PATH:/home/kurmasz/public/CS451/bin/`
* * `export PATH`
* Then run the command
* * `source ~/.bash_profile`